// sdl_main.cpp

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif

#include <sstream>

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_syswm.h>
#undef main

#include <glm/gtc/matrix_transform.hpp>

#include <stdio.h>
#include <string.h>
#include <sstream>

#include "BMFont.h"
#include "ShaderWithVariables.h"
BMFont g_font("../textures/segoeui.fnt");
ShaderWithVariables m_fontShader;

SDL_Window* g_pWindow = NULL;
SDL_GLContext g_pContext = NULL;

void PollEvents()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch(event.type)
        {
        case SDL_QUIT:
            exit(0);
            break;

        case SDL_KEYDOWN:
        case SDL_KEYUP:
            if (event.key.keysym.sym == SDLK_ESCAPE)
            {
                exit(0);
            }
            break;

        default:
            break;
        }
    }
}

void initGL()
{
    // Font Shader
    m_fontShader.initProgram("fontrender");
    m_fontShader.bindVAO();

    GLuint vertVbo = 0;
    glGenBuffers(1, &vertVbo);
    m_fontShader.AddVbo("vPosition", vertVbo);

    glEnableVertexAttribArray(m_fontShader.GetAttrLoc("vPosition"));
    glEnableVertexAttribArray(m_fontShader.GetAttrLoc("vTexCoord"));

    GLuint triVbo = 0;
    glGenBuffers(1, &triVbo);
    m_fontShader.AddVbo("elements", triVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triVbo);

    glBindVertexArray(0);

    g_font.initGL();
}

void display()
{
    glClearColor(.2f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const glm::mat4 modelview(1.f);
    const glm::mat4 projection = glm::ortho(
        0.f,
        static_cast<float>(1000),
        static_cast<float>(800),
        0.f,
        -1.f,
        1.f);

    g_font.DrawString("The quick brown fox", 20, 20, modelview, projection, m_fontShader);
}

int main(int argc, char** argv)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        return false;
    }

    g_pWindow = SDL_CreateWindow(
            "GLwithText",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            1000, 800,
            SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if (g_pWindow == NULL)
    {
        SDL_Quit();
    }

    // thank you http://www.brandonfoltz.com/2013/12/example-using-opengl-3-0-with-sdl2-and-glew/
    g_pContext = SDL_GL_CreateContext(g_pWindow);
    if (g_pContext == NULL)
    {
        return 0;
    }

    SDL_GL_MakeCurrent(g_pWindow, g_pContext);

    // Don't forget to initialize Glew, turn glewExperimental on to
    // avoid problems fetching function pointers...
    glewExperimental = GL_TRUE;
    const GLenum l_Result = glewInit();
    if (l_Result != GLEW_OK)
    {
        exit(EXIT_FAILURE);
    }

    initGL();

    int quit = 0;
    while (quit == 0)
    {
        PollEvents();

        display();
        SDL_GL_SwapWindow(g_pWindow);
    }

    SDL_GL_DeleteContext(g_pContext);
    SDL_DestroyWindow(g_pWindow);

    SDL_Quit();
}
