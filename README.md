# GLwithText #

A simple SDL OpenGL app with font rendering. Useful as a starting point for apps with text.

### Dependencies ###

* glew
* SDL2
* glm
* python
* CMake